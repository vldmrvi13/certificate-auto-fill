import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon
from ui import Ui_MainWindow
from PIL import Image, ImageDraw, ImageFont

class MakeImg(QtWidgets.QMainWindow):
    def __init__(self):
        super(MakeImg, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.init_UI()

    def init_UI(self):
        self.setWindowTitle('Pillow_GUI')
        self.setWindowIcon(QIcon('icon.png'))

        self.ui.img.setPlaceholderText('Путь к шаблону')
        self.ui.file.setPlaceholderText('Путь к файлу с ФИО')
        self.ui.size.setValue(25)
        self.ui.X.setPlaceholderText('*')
        self.ui.Y.setPlaceholderText('*')

        self.ui.create_button.clicked.connect(self.createImg)

    def createImg(self):
        input_image = self.ui.img.text()
        input_file = self.ui.file.text()
        input_font = self.ui.font.text()
        input_size = int(self.ui.size.text())
        input_X = int(self.ui.X.text())
        input_Y = int(self.ui.Y.text())

        font = ImageFont.truetype(input_font, input_size)
        FIO = open(input_file, encoding="utf8")

        for line in FIO:
            line = line.replace("\n", "")
            image = Image.open(input_image)
            drawer = ImageDraw.Draw(image)
            drawer.text((input_X,input_Y), line, font=font, fill='black')
            image.save(line +'.jpg')

app = QtWidgets.QApplication([])
application = MakeImg()
application.show()

sys.exit(app.exec_())