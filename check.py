from PIL import Image, ImageDraw, ImageFont

font = ImageFont.truetype("timesi.ttf", 25) #timesi - Times New Roman, arial - Arial
FIO = open('FIO.txt', encoding="utf8")

for line in FIO:
    line = line.replace("\n","")
    image = Image.open("images.jpg")
    drawer = ImageDraw.Draw(image)
    drawer.text((190, 560), line, font=font, fill='black')
    image.save(line + '.jpg')
    print(line + "--Файл сохранен")

#image.show()